# Simulador de Crédito 

Este repositorio te ayuda a realizar el calculo de un crédito utilizando el método Francés para realiza el calculo


## Clonatelo 

* `git clone <repositorio-a-clonar-url>` Url de este repositorio
* `abrir Index.html`

## Ejecutalo

* Una vez descargado, tienes que abrirlo con tu navegador preferido. Ejemplo: Chrome


## Canal de Youtube
* [EscupiendCódigo](https://youtube.com/channel/UC4w8VIeA7H8xjrASPTuPMxA)

## Apoyame
* Si te gusta el contenido puedes apoyarme invitandome un pastelito
* [ApoyoAlCreador](https://www.paypal.com/paypalme/byjazr)

